all: web gulp

virtualenv:
	test -d .venv || virtualenv .venv
	. .venv/bin/activate; pip install fabric==1.8.1 fabtools PyCrypto
	touch .venv/bin/activate
	
web: virtualenv

clean:
	rm -rf .venv
	
ship:
	. .venv/bin/activate; fab -f src/deploy/fabfile ship
	
mark:
	. .venv/bin/activate; fab -f src/deploy/fabfile mark

gulp:
	@cd src/web/dev/; npm install

run:
	@cd src/web/dev/; NODE_ENV=development gulp serve

build:
	@cd src/web/dev/; NODE_ENV=production gulp build