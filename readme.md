# Mess Default Template v2.2.4

## Dependencies

- Node
- NPM
- Gulp

## Initialization

Since Node modules aren't stored in the repository, you'll need to run `make gulp` to install them.

## Running the project

From the base directory, the command `make run` will spin up a BrowserSync server and initialize the watch tasks for processing SCSS and Javascript, live reloading / style injection, and Javascript hinting.

- By default, it will serve at <http://localhost:3000>.
- More information and settings are available at <http://localhost:3001>
- Hinting results will output to the command line. If there are any errors or warnings, fix them.


## Structure

- `src/web/dev/` contains your gulpfiles and unprocessed source files. This is the directory where you can run `npm` commands.
- `src/web/dev/assets/` is where your unprocessed html, javascript, sass, images, fonts, etc exist.
- `src/web/dev/assets/copy/` contains files that only need to be copied to the build directory without processing. This could be fonts, pdfs, etc.
- `src/web/build/` is the build directory. This is the production code for your site. Don't change anything here -- it will get overwritten.

### Javascript

Un-minified, un-concatenated javascript lives in `src/web/dev/assets/js/`. The directory structure there is relatively self-explanatory; backbone views, models, and collections go in `views/`, `models/`, and `collections/` respectively, handlebars templates in `templates/`, vendor code in `lib/`, and any functions that don't quite fit in a view or model can be put in `helpers/`.

`app.js` is the entry point for your application.

`environments.js` allows you to set environment-specific variables. The environment will be *development* when you are running a local server and *production* when you build for production.

`router.js` is your application's router, and where you should initialize your views, whether via a route or in the initialize function.

You can use NPM to install packages such as jQuery and Backbone: in `src/web/dev/` run `npm install PACKAGE_NAME --save-dev` and you will be able to import it in your scripts using the node require syntax.

## Miscellaneous Notes

### Node Version

Your Node installation must be version 5.x.x. There was a breaking change in v6 that many packages have not caught up with yet.

### jQuery Plugins

Generally we don't use jQuery, but if you need to include a jQuery plugin, you can use this pattern:

```
import $ from 'jquery';
import './path/to/example_plugin';

$('.foo').examplePlugin();

```