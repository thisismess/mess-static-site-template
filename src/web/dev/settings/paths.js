/*
    Note: paths are relative to the gulpfile.
*/

module.exports = {
    assets : './assets/', // unprocessed assets
    base: '../build/', // build directory
    static: '../build/static/', // static assets
}