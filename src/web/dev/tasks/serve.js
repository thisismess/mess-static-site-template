const gulp = require('gulp');
const watch = require('gulp-watch');
const browserSync = require('browser-sync');
const reload = browserSync.reload;
const paths = require('../settings/paths.js');

// watch / reload tasks
gulp.task('serve', ['build'], function() {
    browserSync({
        server: { baseDir: paths.base },
        open: false,
        logConnections: true,
        ghostMode: false,
    });

    // watch scss
    watch(paths.assets + 'scss/**/*.scss', { usePolling: true, interval: 100 }, function() {
        gulp.start(['scss']);
    });
    
    // watch scripts and templates
    watch([
            paths.assets + 'js/**/!(*.min).js',
        ],
        { usePolling: true, interval: 100 },
        function() {
            gulp.start('hint');
        }
    );

    watch([
            paths.assets + 'js/**/*.html',
        ],
        { usePolling: true, interval: 100 },
        function() {
            gulp.start('refresh');
        }
    );
    // gulp.start('bundle');
    
    // watch images
    watch(
        paths.assets + 'img/**/*.{png,jpg,jpeg,gif,svg}',
        { usePolling: true, interval: 100 },
        function() {
            gulp.start(['images']);
        }
    );
    
    // watch copy files
    watch(
        paths.assets + 'copy/**/*',
        { usePolling: true, interval: 100 },
        function() {
            gulp.start(['copy']);
        }
    );

    // watch html files
    watch(
        paths.assets + 'html/**/*',
        { usePolling: true, interval: 100 },
        function() {
            gulp.start(['html'], reload);
        }
    );

});
