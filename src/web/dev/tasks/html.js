var gulp = require('gulp');
var fileInclude = require('gulp-file-include');
var changed = require('gulp-changed');
var preprocess = require('gulp-preprocess');
var plumber = require('gulp-plumber');
var paths = require('../settings/paths');
var onError = require('../settings/plumber.js').onError;

gulp.task('html', function() {
    var timestamp = Math.floor(Date.now() / 1000);
    
    return gulp.src([
            paths.assets + 'html/**/*.html',
            '!' + paths.assets + 'html/partials/**/*.html',
        ])
        .pipe(plumber({
            errorHandler: onError,
        }))
        // .pipe(changed(paths.base)) // doesn't update when partials are changed
        .pipe(fileInclude({
            prefix: '@@',
            basepath: '@file',
        }))
        .pipe(preprocess({
            context: {
                ENVIRONMENT: process.env.NODE_ENV,
                DEBUG: true,
                TIMESTAMP: timestamp
            }
        }))
        .pipe(gulp.dest(paths.base));
});