const gulp = require('gulp');
const browserSync = require('browser-sync');
const assign = require('lodash.assign');
const browserify = require('browserify');
const watchify = require('watchify');
const jshint = require('gulp-jshint');
const uglify = require('gulp-uglify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const babelify = require('babelify').configure({
    presets: [
        '@babel/preset-env',
    ],
});
const stringify = require('stringify');

// settings & configuration
const jsHintSettings = require('../settings/jshint');
const paths = require('../settings/paths');
const onError = require('../settings/plumber.js').onError;

// envs
const environments = require('gulp-environments');
const development = environments.development;
const production = environments.production;

const customOpts = {
    entries: [paths.assets + 'js/app.js'],
    paths: [
        paths.assets + 'js',
        paths.base + 'node_modules',
    ],
    fullPaths: true,
    debug: !production(),
    poll: true,
    transform: [
        [ babelify ],
        [ stringify ],
        [
            'envify',
            {
                global: true,
                _: 'purge',
                NODE_ENV: process.env.NODE_ENV
            }
        ]
    ],
};
var opts = assign({}, watchify.args, customOpts);
if (production()) { opts.fullPaths = false; }
var b = production() ? browserify(opts) : watchify(browserify(opts));


gulp.task('bundle', bundle);
gulp.task('refresh', ['bundle'], browserSync.reload);
b.on('update', function() {
    gulp.start('refresh');
});
b.on('log', console.log);


function bundle() {
    return b.bundle()
        .on('error', function(err) {
            console.error(err);
            this.emit('end');
        })
        .pipe(source('site.min.js'))
        .pipe(buffer())
        .pipe(production(uglify()))
        .pipe(gulp.dest(paths.static + 'js'));
}

gulp.task('hint', function() {
    return gulp.src([
            paths.assets + 'js/**/*.js',
            "!" + paths.assets + 'js/lib/**', // don't hint lib files
        ])
        .pipe(jshint(jsHintSettings))
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'));
});
