var gulp = require('gulp');
var changed = require('gulp-changed');
var plumber = require('gulp-plumber');
var paths = require('../settings/paths.js');

gulp.task('copy', function() {
    return gulp.src([
            paths.assets + 'copy/**/*',
        ])
        .pipe(plumber())
        .pipe(changed(paths.static))
        // save to static
        .pipe(gulp.dest(paths.static));
});