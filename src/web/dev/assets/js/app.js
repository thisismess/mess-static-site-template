import Vue from 'vue/dist/vue.common.js';

import router from './router';
import store from './store';

import env from './environments';

/*
    This is the root Vue instance.
*/

console.log('Starting App');

window.app = new Vue({
    router: router,
    store: store,
    
}).$mount('#app');