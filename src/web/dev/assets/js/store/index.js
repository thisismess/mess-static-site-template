import Vue from 'vue/dist/vue.common.js';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    modules: {},
});
