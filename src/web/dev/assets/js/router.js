import Vue from 'vue/dist/vue.common.js';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [];

let router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'active',
    routes: routes,
});

export default router;