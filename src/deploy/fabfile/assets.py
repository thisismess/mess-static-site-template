from fabric.api import *
from .utils import log


def build_assets():
    log('Building static assets...')
    with lcd(env.local_dev_path):
        local('NODE_ENV=%s gulp build' % env.node_env)

def git_release():
    log('Committing %s build to git...' % env.env_name)
    with settings(warn_only=True):
        local('git add --all .')
        local('git commit -am "%s build"' % env.env_name)
        local('git push')

def ship_assets():
    execute(build_assets)
    execute(git_release)