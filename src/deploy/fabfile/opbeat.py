from fabric.api import runs_once, lcd, local, task

def register_deployment(git_path): 
    with(lcd(git_path)): 
        local("""
            curl https://opbeat.com/api/v1/organizations/764a5c0433df47eca852e35ccb36e1c1/apps/dda19637bf/releases/ \
                -H "Authorization: Bearer 8d468c01116d15a09b08e4e91ad83ecd3adc86dc" \ 
                -d rev=`git log -n 1 --pretty=format:%H` \ 
                -d branch=`git rev-parse --abbrev-ref HEAD` \ 
                -d status=completed
        """)