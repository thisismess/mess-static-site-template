from fabric.api import *
import os

# NEVER STORE PASSWORDS IN THIS FILE. SERIOUSLY. I WILL HUNT YOU DOWN.

CURRENT_ENVS = ('live', 'staging',)

def live():
    # ############################################
    # Project Configuration (You should only HAVE to change these)
    env.env_name = 'live'
    env.project = ''  # the name of things
    env.hosts = ["", ]
    env.site = ''  # treat like Nginx, space between hosts
    env.site_protect = True
    env.uses_celeryd = False
    env.keep_releases = 5
    env.branch = 'live'
    env.node_env = 'production'
    env.sd_agent = '2d7fdeef3f42c88b072e7de7ac798415'
    common()


def staging():
    # ############################################
    # Project Configuration (You should only HAVE to change these)
    env.env_name = 'staging'
    env.project = ''  # the name of things
    env.hosts = ["", ]
    env.site = ''  # treat like Nginx, space between hosts
    env.site_protect = True
    env.uses_celeryd = False
    env.keep_releases = 5
    env.branch = 'staging'
    env.node_env = 'staging'
    env.sd_agent = '2d7fdeef3f42c88b072e7de7ac798415'
    common()


def common():
    # ############################################
    # YOU SHOULD NOT HAVE TO CHANGE ANYTHING BELOW
    # Database Settings
    env.django_app_path = 'src/web/build'
    env.db_host = 'localhost'
    env.db_user = 'mess'
    env.db = '%s' % env.project
    # Server Settings
    env.user = 'mess'
    env.cwd = "/home/mess/sites/%s" % env.project
    env.remote_name = 'public'
    env.remote_path = os.path.join(env.cwd, env.remote_name)
    env.remote_virtualenv = os.path.join(env.cwd, 'env')
    env.remote_statics_path = os.path.join(env.remote_path, env.project)
    env.remote_uploads_path = os.path.join(env.cwd, 'uploads')
    env.remote_backups_path = os.path.join(env.cwd, 'backups')
    env.remote_logs_path = os.path.join(env.cwd, 'logs')
    # Local settings
    env.local_name = 'web/build'
    env.repo_root = os.path.join(os.path.dirname(__file__), '../../../')
    env.local_path = os.path.join(env.repo_root, env.django_app_path)
    env.local_release_path = os.path.join(env.local_path, env.local_name)
    env.local_nginx_config = os.path.join(env.repo_root, 'etc', 'nginx.conf')
    env.local_sd_config = os.path.join(env.repo_root, 'etc', 'sd-agent.cfg')
    env.local_dev_path = os.path.join(env.repo_root, 'src/web/dev')
    if 'django_app' in env:
        env.local_uploads_path = os.path.join(env.local_path, env.django_app, 'uploads')
        env.local_uwsgi_config = os.path.join(env.repo_root, 'etc', 'uwsgi.conf')
        env.local_uwsgi_ini = os.path.join(env.repo_root, 'etc', 'uwsgi.ini')
    if 'uses_celeryd' in env and env.uses_celeryd:
        env.local_celery_config = os.path.join(env.repo_root, 'etc', 'celery.conf')
    env.local_virtualenv = os.path.join(env.repo_root, '.venv/bin/activate')
    # Releases
    env.remote_release_path = "%s/releases" % env.cwd    